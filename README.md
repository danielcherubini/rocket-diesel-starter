# Rocket Diesel Starter

You're going to need to have a Postgres server running somewhere, then copy `env.example` into `.env` and edit the file.

From there you can run `cargo run` to run the server

To inject data 

```curl -X POST http://localhost:8000/api -H "Content-Type: application/json" -d '{"title": "foo", "text": "foo foo foo", "published": true }'```
