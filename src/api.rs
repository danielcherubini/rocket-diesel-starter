use crate::posts::{self, Database, Post};

use rocket::{
    fairing::AdHoc,
    response::{status::Created, Debug},
    serde::json::Json,
};

use rocket_sync_db_pools::diesel;

type Result<T, E = Debug<diesel::result::Error>> = std::result::Result<T, E>;

#[post("/post", data = "<post>")]
async fn create(db: Database, post: Json<Post>) -> Result<Created<Json<Post>>> {
    let post_value = post.clone();
    let new_post = posts::create_post(db, post_value).await?;
    Ok(Created::new("/").body(Json(new_post)))
}

#[get("/post")]
async fn list(db: Database) -> Result<Json<Vec<i32>>> {
    let ids = posts::all_posts(db).await?;

    Ok(Json(ids))
}

#[get("/post/<id>")]
async fn read(db: Database, id: i32) -> Option<Json<Post>> {
    posts::read_post_with_id(db, id).await.map(Json).ok()
}

#[delete("/post/<id>")]
async fn delete(db: Database, id: i32) -> Result<Option<()>> {
    let affected = posts::delete_post_with_id(db, id).await?;

    Ok((affected == 1).then(|| ()))
}

#[delete("/post")]
async fn destroy(db: Database) -> Result<()> {
    posts::destroy_all_posts(db).await?;
    Ok(())
}

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Add Api Routes", |rocket| async {
        rocket.mount("/api", routes![list, read, create, delete, destroy])
    })
}
