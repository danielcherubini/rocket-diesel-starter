use std::env;

use self::diesel::prelude::*;
use diesel::result::Error;
use dotenv::dotenv;
use rocket::fairing::AdHoc;
use rocket::figment::map;
use rocket::figment::value::Map;
use rocket::figment::value::Value;
use rocket::serde::{Deserialize, Serialize};
use rocket::{Build, Rocket};
use rocket_sync_db_pools::diesel;

#[database("servers_db")]
pub struct Database(diesel::PgConnection);

#[derive(Debug, Clone, Deserialize, Serialize, Queryable, Insertable)]
#[serde(crate = "rocket::serde")]
#[table_name = "posts"]
pub struct Post {
    #[serde(skip_deserializing)]
    id: Option<i32>,
    title: String,
    text: String,
    #[serde(skip_deserializing)]
    published: bool,
}

table! {
    posts (id) {
        id -> Nullable<Integer>,
        title -> Text,
        text -> Text,
        published -> Bool,
    }
}

pub async fn create_post(db: Database, post: Post) -> Result<Post, Error> {
    let post_value = post.clone();
    db.run(move |conn| {
        diesel::insert_into(posts::table)
            .values(&post_value)
            .get_result::<Post>(conn)
    })
    .await
}

pub async fn all_posts(db: Database) -> Result<Vec<i32>, Error> {
    let ids = db
        .run(move |conn| posts::table.select(posts::id).load(conn))
        .await?;

    let mut new_ids: Vec<i32> = Vec::new();
    for id in ids {
        match id {
            Some(id) => new_ids.push(id),
            None => todo!(),
        }
    }

    Ok(new_ids)
}

pub async fn read_post_with_id(db: Database, id: i32) -> Result<Post, Error> {
    db.run(move |conn| posts::table.filter(posts::id.eq(id)).first(conn))
        .await
}

pub async fn delete_post_with_id(db: Database, id: i32) -> Result<usize, Error> {
    db.run(move |conn| {
        diesel::delete(posts::table)
            .filter(posts::id.eq(id))
            .execute(conn)
    })
    .await
}

pub async fn destroy_all_posts(db: Database) -> Result<usize, Error> {
    db.run(move |conn| diesel::delete(posts::table).execute(conn))
        .await
}

async fn run_migrations(rocket: Rocket<Build>) -> Rocket<Build> {
    // This macro from `diesel_migrations` defines an `embedded_migrations`
    // module containing a function named `run` that runs the migrations in the
    // specified directory, initializing the database.
    embed_migrations!("migrations");

    let conn = Database::get_one(&rocket)
        .await
        .expect("database connection");
    conn.run(|c| embedded_migrations::run(c))
        .await
        .expect("diesel migrations");

    rocket
}

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Diesel Postgres Stage", |_rocket| async {
        dotenv().ok();

        let db_url = env::var("DATABASE_URL").unwrap();
        let db: Map<_, Value> = map! {
            "url" => db_url.into(),
            "pool_size" => 10.into()
        };

        let figment =
            rocket::config::Config::figment().merge(("databases", map!["servers_db" => db]));

        rocket::custom(figment)
            .attach(Database::fairing())
            .attach(AdHoc::on_ignite("Diesel Migrations", run_migrations))
    })
}
