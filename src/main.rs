#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_sync_db_pools;
#[macro_use]
extern crate diesel_migrations;
#[macro_use]
extern crate diesel;

#[cfg(test)]
mod tests;

mod api;
mod posts;
mod web;
#[launch]
fn rocket() -> _ {
    rocket::build()
        .attach(posts::stage())
        .attach(api::stage())
        .attach(web::stage())
}
