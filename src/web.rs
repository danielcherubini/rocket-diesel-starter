use askama::Template;
use rocket::{
    fairing::AdHoc,
    fs::NamedFile,
    response::{content, status::NotFound},
};
use std::path::PathBuf;

use crate::posts::{self, Database};

#[derive(Template)]
#[template(path = "hello.html")]
struct HelloTemplate<'a> {
    title: &'a str,
    name: &'a str,
    ids: Vec<i32>,
}

#[get("/<path..>")]
async fn static_files(path: PathBuf) -> Result<NamedFile, NotFound<String>> {
    let path = PathBuf::from("site").join(path);
    NamedFile::open(path)
        .await
        .map_err(|e| NotFound(e.to_string()))
}

#[get("/")]
async fn index(db: Database) -> Result<content::Html<String>, NotFound<String>> {
    let ids = posts::all_posts(db).await;

    let mut _new_ids: Vec<i32> = Vec::new();

    match ids {
        Ok(ids) => _new_ids = ids,
        Err(_ids) => todo!(),
    }

    let hello = HelloTemplate {
        title: "Hello World",
        name: "world",
        ids: _new_ids,
    };

    let response = content::Html(hello.to_string());
    Ok(response)
}

pub fn stage() -> AdHoc {
    AdHoc::on_ignite("Diesel Postgres Stage", |rocket| async {
        rocket.mount("/", routes![index, static_files])
    })
}
